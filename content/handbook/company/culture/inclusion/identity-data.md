---
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
---

## GitLab Identity Data

Below is a list of the different ways GitLab Team Members choose to identify. The purpose of this page is to give an overview of our current representation as compared to a year ago. For further trending information, some of these metrics can also be viewed on the [People Success Performance Indicators](/handbook/people-group/people-success-performance-indicators/) page.

### Region Data

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/Region" />}}

### Gender Data

#### Gender Identification (Global)

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/Gender" />}}

#### Gender in Management (Global)

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/GenderMgmt" />}}

#### Gender in Leadership (Global)

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/GenderLeadership" />}}

#### Gender in Tech (Global)

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/GenderTech" />}}

#### Gender in Non-Tech (Global)

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/GenderNon-Tech" />}}

#### Gender in Sales (Global)

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/GenderSales" />}}

### Underrepresented Groups (URG) Data - Global**

#### URG at GitLab

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/URG" />}}

#### URG in Management

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/URGMgmt" />}}

#### URG in Leadership

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/URGLeadership" />}}

#### URG in Tech

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/URGTech" />}}

#### URG in Non-Tech

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/URGNon-Tech" />}}

#### URG in Sales

{{< tableau height="200px" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DraftDIBIdentityDashboard/URGSales" />}}

**Source**: GitLab's People Analytics Team, WorkDay
**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**As of 2023-11-01 underrepresented groups for ethnicity are reported globally as opposed to just for team members in the US. This decision was made to better align with GitLab values and to show true representation of team members globally.

Due to data and or legal limitations, this is not an exhaustive list of all of our underrepresented groups (those with disabilities, those that identify as LGBTQIA+, etc. who choose not to disclose).
