---
aliases: /handbook/legal/Vendor-DPA
title: "Vendor Data Processing Addendum"
description: "This agreement ..."
---

<!--
<iframe src="https://gitlab.com/gitlab-com/legal-and-compliance/-/raw/master/Vendor_DPA_and_Standard_Contractual_Clauses.pdf" style="width:100%;" height="900"></iframe>
-->

<a href="Vendor_DPA_and_Standard_Contractual_Clauses.pdf" download="UK_Modern_Slavery_Act_Statement_FY2023.pdf" class="h2">
    <i class="fa-solid fa-file-arrow-down"></i> GitLab Data Processing Addendum and Standard Contractual Clauses</a>
